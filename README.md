* Usage

```zsh
mkvirtualenv -a . cubing -r requirements.txt
<cstimer_*.txt(.om[1]) jq -f jqtils/latest_session.jq -r
# returns e.g. session167
<cstimer_*.txt(.om[1]) \
  jq -f jqtils/session_times.jq --arg session 167 \
  | jq -f jqtils/to_csv.jq -r \
  | ./ao5.py
```

![screenshot of jq pipe into ao5.py](./doc/screenshot_ao5.png)
