# coding: utf-8
import csv
from functools import partial

import pandas as pd
import rich
import rich.table


def ao5(times_window: pd.Series) -> float:
    return times_window.sort_values().iloc[1:-1].mean()


def add_ao5(df: pd.DataFrame) -> pd.DataFrame:
    df["ao5"] = df.sort_values("timestamp").rolling(5)["time"].agg(ao5)
    return df


def report_times(times, title="times") -> rich.table.Table:
    t = rich.table.Table(
        rich.table.Column("#", justify="right"),
        "Penalty",
        rich.table.Column("Time [s]", justify="right"),
        "Scramble",
        title=title,
    )
    for time in times:
        t.add_row(
            str(time.Index), str(time.penalty), f"{(time.time/1000):.3f}", time.scramble
        )
    return t


def report_ao5(sess: pd.DataFrame, i: int):
    rich.print(
        report_times(
            sess.iloc[i - 5 : i]["penalty time scramble".split()].itertuples(),
            title=f"{(sess.iloc[i].ao5/1000):.2f} ao5",
        )
    )


def top_n(df: pd.DataFrame, key: str, n=5) -> pd.DataFrame:
    return df.sort_values(key).iloc[:n]


def main():
    import sys

    file = sys.argv[1] if len(sys.argv) > 1 else sys.stdin
    sess = pd.read_csv(
        file,
        header=0,
        quoting=csv.QUOTE_NONNUMERIC,
        parse_dates=["timestamp"],
        date_parser=partial(pd.to_datetime, unit="s"),
    ).pipe(add_ao5)

    for i in top_n(sess, "ao5", n=2).index:
        report_ao5(sess, i)


if __name__ == "__main__":
    main()
