[
  .["session" + $session]
  |.[]
  |{
    "time": .[0][1],
    "penalty": .[0][0],
    "timestamp": .[3],
    "scramble": .[1]
  }
]
